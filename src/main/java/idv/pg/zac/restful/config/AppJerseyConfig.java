package idv.pg.zac.restful.config;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import idv.pg.zac.restful.controller.ComponentController;

@Configuration
@Component
public class AppJerseyConfig extends ResourceConfig {

	
	public AppJerseyConfig() {
		register(ComponentController.class);
		//register(ComponentService.class);
	}
}
