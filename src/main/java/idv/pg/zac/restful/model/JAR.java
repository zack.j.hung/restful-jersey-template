package idv.pg.zac.restful.model;

import org.springframework.stereotype.Component;

import idv.pg.zac.restful.service.ComponentService;

@Component("jarService")
public class JAR implements ComponentService {

	@Override
	public String getVersion() {
		return "1.3.9";
	}

}
