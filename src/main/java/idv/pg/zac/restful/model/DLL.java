package idv.pg.zac.restful.model;

import org.springframework.stereotype.Component;

import idv.pg.zac.restful.service.ComponentService;

@Component(value = "dllService")
public class DLL implements ComponentService {

	@Override
	public String getVersion() {
		return "2.1.8";
	}

}
