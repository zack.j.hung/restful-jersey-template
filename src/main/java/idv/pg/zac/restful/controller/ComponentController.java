package idv.pg.zac.restful.controller;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.PathVariable;

import idv.pg.zac.restful.service.ComponentService;

@Path("/component")
public class ComponentController {

	@Autowired
	@Qualifier("dllService")
	private ComponentService comDLLService;
	
	@Autowired
	@Qualifier("jarService")
	private ComponentService comJARService;
	
	@GET @Path("/dll")
	@Produces(MediaType.TEXT_PLAIN)
	public String ComponentDLLInfo() {
		return comDLLService.getVersion();
	}
	
	@GET @Path("/jar")
	@Produces(MediaType.TEXT_PLAIN)
	public String ComponentJARInfo() {
		return comJARService.getVersion();
	}
}
