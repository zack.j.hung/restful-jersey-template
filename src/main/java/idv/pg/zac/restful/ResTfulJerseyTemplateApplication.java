package idv.pg.zac.restful;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({"idv.pg.zac.restful.service", "idv.pg.zac.restful.config", "idv.pg.zac.restful.model"})
public class ResTfulJerseyTemplateApplication /*extends SpringBootServletInitializer*/ {

	public static void main(String[] args) {
		//new ResTfulJerseyTemplateApplication().configure(
			//	new SpringApplicationBuilder(ResTfulJerseyTemplateApplication.class)).run(args);
				
		SpringApplication.run(ResTfulJerseyTemplateApplication.class, args);
	}

}
